import React from 'react'
import './App.css';
import Expenses from './components/Expenses';

function App() {
  const expenses = [
    {
      id: 1,
      title: "Fast Food",
      amount: 450,
      date: new Date(2021, 2, 21)
    },
    {
      id: 2,
      title: "Hamburger",
      amount: 50,
      date: new Date(2021, 5, 10)
    },
    {
      id: 3,
      title: "McDonalds",
      amount: 600,
      date: new Date(2021, 6, 1)
    }
  ];



   // BAD WAY
  // return React.createElement(
  //   'div', 
  //   {}, 
  //   React.createElement('h2', {}, 'Let\'s get started.'),
  //   React.createElement(Expenses, {expenses:expenses})
  // )

  // PERFECT WAY
  return (
    <div>
      <h2>Let's get started.</h2>
      <Expenses expenses={expenses} />
    </div>
  );
}

export default App;
