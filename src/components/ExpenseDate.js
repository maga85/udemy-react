import React from 'react'

const ExpenseDate = (props) => {
    const month = props.date.toLocaleString('en-US', { month: 'long' });
    const day = props.date.toLocaleString('en-US', { day: '2-digit' })
    const year = props.date.getFullYear()
    return (
        <>
            <em>{month}</em> <br />
            <em>{day}</em><br />
            <em>{year}</em><br />
        </>
    )
}

export default ExpenseDate
